/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nav.util.string;

import java.util.regex.*;

/**
 * A collection of String utilities.
 *
 * @author Benjamin Jakobus, based on work by Cormac Gebruers, Transas Marine.
 * @version 1.0
 * @since 1.0
 */
public class StringUtil {

    /**
     * Splits a newline (\n) delimited string into an array of strings.
     * Note: <code>StringTokenizer</code> may be used instead.
     *
     * @param str               The string to split up.
     * @param delimiter         The delimiter to use in splitting.
     * @returns                 An array of String objects equivalent to str.
     * @since 1.0
     */
    public String[] splitDelimitedStr(String str, String delimiter) {
        Pattern pttn = Pattern.compile(delimiter);
        return pttn.split(str);
    }

    /**
     * Right aligns a long number with spaces for printing.
     *
     * @param num           The number to be aligned.
     * @param totalLen      The total length of the padded string.
     * @return              The padded number as a <code>String</code>.
     * @since 1.0
     */
    public String padNum(long num, int totalLen) {
        String numStr = new Long(num).toString();
        int len = totalLen - numStr.length();
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += " ";
        }
        return pads + numStr;
    }

    /**
     * Right aligns a long number with zeros for printing
     *
     * @param num           The number to be aligned.
     * @param totalLen      The total length of the padded string.
     * @return              The padded number.
     * @since 1.0
     */
    public String padNumZero(long num, int totalLen) {
        String numStr = new Long(num).toString();
        int len = totalLen - numStr.length();
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += "0";
        }
        return pads + numStr;
    }

    /**
     * Right aligns an integer number with spaces for printing.
     *
     * @param num           The number to be aligned.
     * @param totalLen      The total length of the padded string.
     * @return              The padded number.
     */
    public String padNum(int num, int totalLen) {
        String numStr = new Integer(num).toString();
        int len = totalLen - numStr.length();
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += " ";
        }
        return pads + numStr;
    }

    /**
     * Right aligns an integer number with zeros for printing.
     *
     * @param num           The number to be aligned.
     * @param totalLen      The total length of the padded string.
     * @return              The padded number.
     */
    public String padNumZero(int num, int totalLen) {
        String numStr = new Long(num).toString();
        int len = totalLen - numStr.length();
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += "0";
        }
        return pads + numStr;
    }

    /**
     * Right aligns a double number with spaces for printing.
     *
     * @param num           The number to be aligned.
     * @param wholeLen      The total length of the padded string.
     * @return              The padded number.
     * @since 1.0
     */
    public String padNum(double num, int wholeLen, int decimalPlaces) {
        String numStr = new Double(num).toString();
        int dpLoc = numStr.indexOf(".");

        int len = wholeLen - dpLoc;
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += " ";
        }

        numStr = pads + numStr;

        dpLoc = numStr.indexOf(".");

        if (dpLoc + 1 + decimalPlaces > numStr.substring(dpLoc).length()) {
            return numStr;
        }
        return numStr.substring(0, dpLoc + 1 + decimalPlaces);
    }

    /**
     * Right aligns a double number with zeros for printing.
     *
     * @param num           The number to be aligned.
     * @param wholeLen      The total length of the padded string.
     * @return              The padded number.
     * @since 1.0
     */
    public String padNumZero(double num, int wholeLen, int decimalPlaces) {
        String numStr = new Double(num).toString();
        int dpLoc = numStr.indexOf(".");

        int len = wholeLen - dpLoc;
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += "0";
        }

        numStr = pads + numStr;

        dpLoc = numStr.indexOf(".");

        if (dpLoc + 1 + decimalPlaces > numStr.substring(dpLoc).length()) {
            return numStr;
        }
        return numStr.substring(0, dpLoc + 1 + decimalPlaces);
    }

    /**
     * Right aligns a float number with spaces for printing.
     *
     * @param num           The number to be aligned.
     * @param wholeLen      The total length of the padded string.
     * @return              The padded number.
     * @since 1.0
     */
    public String padNum(float num, int wholeLen, int decimalPlaces) {
        String numStr = new Float(num).toString();
        int dpLoc = numStr.indexOf(".");

        int len = wholeLen - dpLoc;
        String pads = "";
        for (int i = 0; i < len; i++) {
            pads += " ";
        }

        numStr = pads + numStr;

        dpLoc = numStr.indexOf(".");

        if (dpLoc + 1 + decimalPlaces > numStr.substring(dpLoc).length()) {
            return numStr;
        }
        return numStr.substring(0, dpLoc + 1 + decimalPlaces);
    }

}
