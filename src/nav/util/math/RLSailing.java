/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nav.util.math;

/**
 * A wrapper class to package up a rhumb line sailing.
 * 
 * @author Benjamin Jakobus, based on JMarine (by Cormac Gebruers and Benjamin
 *          Jakobus)
 * @version 1.0
 * @since 1.0
 */
public class RLSailing {

    private double course;
    private double distNM;

    /**
     * Constructor.
     *
     * @param pCourses          <code>Double</code> denoting the course in degrees
     *                          (0 to 359).
     * @param pDistancesNM      <code>Double</code> denoting the distance in
     *                          nautical miles.
     * @since 1.0
     */
    public RLSailing(double pCourse, double pDistNM) {
        course = pCourse;
        distNM = pDistNM;
    }

    /**
     * Returns sailing course in degrees.
     *
     * @return          <code>Double</code> denoting the course in degrees
     *                  (0 to 359).
     * @since 1.0
     */
    public double getCourse() {
        return course;
    }

    /**
     * Returns the sailing distance in nautical miles.
     *
     * @return          <code>Double</code> denoting the distance in
     *                  nautical miles.
     * @since 1.0
     */
    public double getDistNM() {
        return distNM;
    }
}
