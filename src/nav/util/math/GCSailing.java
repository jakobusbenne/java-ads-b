/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nav.util.math;

/**
 * A wrapper class to package up a great circle sailing.
 * 
 * @author Benjamin Jakobus, based on JMarine (by Cormac Gebruers and Benjamin
 *          Jakobus)
 *
 * @version 1.0
 * @since 1.0
 */
public class GCSailing {
    
    /* Courses in degrees. */
    private int[] courses;

    /* Distances in nautical miles. */
    private float[] distancesNM;

    /**
     * Constructor.
     *
     * @param pCourses          <code>Integer</code> array containing courses in degrees
     *                          (0 to 359).
     * @param pDistancesNM      <code>Float</code> array containing distances in
     *                          nautical miles.
     * @since 1.0
     */
    public GCSailing(int[] pCourses, float[] pDistancesNM) {
        courses = pCourses;
        distancesNM = pDistancesNM;
    }

    /**
     * Returns sailing courses in degrees.
     *
     * @return          <code>Integer</code> array containing courses in degrees
     *                  (0 to 359).
     * @since 1.0
     */
    public int[] getCourses() {
        return courses;
    }

    /**
     * Returns the sailing distances in nautical miles.
     *
     * @return          <code>Float</code> array containing distances in
     *                  nautical miles.
     * @since 1.0
     */
    public float[] getDistancesNM() {
        return distancesNM;
    }
}
