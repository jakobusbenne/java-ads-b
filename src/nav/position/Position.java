/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nav.position;

import exception.InvalidPositionException;

/**
 * This class represents the position of an entity in the world.
 * 
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class Position {

    /* the latitude (+ N/E) */
    private Coordinate lat;

    /* the longitude  (-W/S) */
    private Coordinate lng;

    /* An optional time to associate with this position - for historical tracking */
    private String utcTimeStamp;

    /* Degree position */
    private double degree;

    /**
     * A new position expressed in decimal format.
     * 
     * @param dblLat            <code>Double</code> denoting the Latitude.
     * @param dblLng            <code>Double</code> denoting the Longitude.
     * @since 1.0
     */
    public Position(double dblLat, double dblLng) throws InvalidPositionException {
        lat = new Coordinate(dblLat, Coordinate.LAT);
        lng = new Coordinate(dblLng, Coordinate.LNG);
    }

    /**
     * A new position expressed in ALRS format.
     * 
     * @param lat
     * @param lng
     * @since 1.0
     */
    public Position(String lat, String lng) throws InvalidPositionException {
        this.lat = new Coordinate(lat);
        this.lng = new Coordinate(lng);
    }

    /**
     * A new position expressed in NMEA GPS message format:
     * 4807.038,N,01131.000,E
     * @param
     * @param
     * @param
     * @param
     * @since  12.0
     */
    public Position(String latNMEAGPS, String latQuad, String lngNMEAGPS, String lngQuad, String utcTimeStamp) {
        int quad;

        //LAT
        if (latQuad.compareTo("N") == 0) {
            quad = Coordinate.N;
        } else {
            quad = Coordinate.S;
        }
        try {
            this.lat = new Coordinate(Integer.valueOf(latNMEAGPS.substring(0, 2)), Float.valueOf(latNMEAGPS.substring(2)), Coordinate.LAT, quad);
        } catch (InvalidPositionException e) {
            e.printStackTrace();
        }

        //LNG
        if (lngQuad.compareTo("E") == 0) {
            quad = Coordinate.E;
        } else {
            quad = Coordinate.W;
        }
        try {
            this.lng = new Coordinate(Integer.valueOf(lngNMEAGPS.substring(0, 3)), Float.valueOf(lngNMEAGPS.substring(3)), Coordinate.LNG, quad);
        } catch (InvalidPositionException e) {
            e.printStackTrace();
        }

        //TIMESTAMP
        this.associateUTCTime(utcTimeStamp);
    }

    /**
     * Add a reference time for this position - useful for historical tracking.
     *
     * @param time              The timestamp.
     * @since 1.0
     */
    public void associateUTCTime(String time) {
        utcTimeStamp = time;
    }

    /**
     * Returns the UTC time stamp.
     *
     * @return str          The UTC timestamp associated with this <code>Position</code>
     *                      instance.
     * @since 1.0
     */
    public String utcTimeStamp() {
        return utcTimeStamp;
    }

    /**
     * Prints out position using decimal format.
     * 
     * @return              The position in decimal format.
     * @since 1.0
     */
    public String toStringDec() {
        return lat.toStringDec() + " " + lng.toStringDec();
    }

    /**
     * Return the latitude in decimal format.
     * 
     * @return              The latitude in decimal format.
     * @since 1.0
     */
    public double getLatitude() {
        return lat.decVal();
    }

    /**
     * Return the position longitude in decimal format.
     *
     * @return              The longitude in decimal format.
     * @since 1.0
     */
    public double getLongitude() {
        return lng.decVal();
    }

    /**
     * Prints out position using DegMin format.
     *
     * @return              The position in DegMin Format.
     * @since 1.0
     */
    public String toStringDegMin() {
        String output = "";
        output += lat.toStringDegMin();
        output += "   " + lng.toStringDegMin();
        return output;
    }

    /**
     * Returns the latitude as a <code>String</code> in degrees and minutes.
     * 
     * @return              The latitude as a <code>String</code>.
     * @since 1.0
     */
    public String toStringDegMinLat() {
        return lat.toStringDegMin();
    }

    /**
     * Returns the longitude as a <code>String</code> in degrees and minutes.
     *
     * @return              The longitude as a <code>String</code>.
     * @since 1.0
     */
    public String toStringDegMinLng() {
        return lng.toStringDegMin();
    }

    /**
     * Returns the latitude as a <code>String</code> in decimal format.
     * .
     * @return              The latitude as a <code>String</code>.
     * @since 1.0
     */
    public String toStringDecLat() {
        return lat.toStringDec();
    }

    /**
     * Returns the longitude as  a <code>String</code> in decimal format.
     *
     * @return              The longitude as a <code>String</code>.
     * @since 1.0
     */
    public String toStringDecLng() {
        return lng.toStringDec();
    }

    @Override
    public boolean equals(Object obj) {
        Position p2 = (Position) obj;
        if (p2.getLatitude() == this.getLatitude()
                && p2.getLongitude() == this.getLongitude()) {
            return true;
        }
        return false;
    }

    //TEST HARNESS - DO NOT DELETE!
    public static void main(String[] argsc) {

        //NMEA GPS Position format:
        Position p = new Position("4807.038", "N", "01131.000", "W", "123519");
        System.out.println(p.toStringDegMinLat());
        System.out.println(p.getLatitude());
        System.out.println(p.getLongitude());
        System.out.println(p.toStringDegMinLng());
        System.out.println(p.utcTimeStamp());
        try {
            Position p1 = new Position(0.2, 0.1);
            Position p2 = new Position(0.1, 0.1);
            System.out.println(p1.equals(p2));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
