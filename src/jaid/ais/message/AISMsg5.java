/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jaid.ais.message;

import nav.util.string.StringUtil;

/**
 * Message type 5 are report messages used by Class A vessels when reporting
 * static or voyage related data.For details see ITU-R M.1371-3.
 * 
 * @author Benjamin Jakobus
 * @version 1.0
 * @since 1.0
 */
public class AISMsg5 extends AISMsg {

    /* AIS version indicator. */
    private int aisVersionInd;

    /* IMO Number. */
    private int imoNum;

    /* Call-sign. */
    private String callsign;

    /* Vessel type. */
    private String vlTypeCargo;

    /* ETA. */
    private String eta;

    /* Maximum Static Draft. */
    private float maxDraft;

    /* Destination. */
    private String destination;
                                                    
    /**
     * Constructs a new AIS message type 5.
     * 
     * @param data      The AIS feed to parse.
     * @since 1.0
     */
    public AISMsg5(String data) {
        // Message type (bits 0-5)
	super.setMessageType(Integer.parseInt(data.substring(0,6),2));

        // Repeat Indicator (6-8)
	super.setRepeatIndicator(Integer.parseInt(data.substring(6,8),2));

	// MMSI (9-38)
	super.setMmsi(String.valueOf(Integer.parseInt(data.substring(8,38), 2)));

	// IMO Number (41-70)
	imoNum = Integer.parseInt(data.substring(40,70),2);

	// Call sign (70-112)
	callsign = super.decode6BitBinary(data.substring(70,112));

	// Name (112-232)
	super.decodeName(data.substring(112,232));

	// Vessel Type & Cargo (232-240)
	decodeVesselTypeCargo(data.substring(232,240));

	// Vessel dimensions (240-270)
	super.decodeEntityDimensions(data.substring(240,270));

	// EPFS type (270-274)
	super.decodeEPFSType(data.substring(270,274));

	// ETA (274-294)
	decodeETA(data.substring(274,294));

	// Draft (294-302)
	maxDraft = (float)Integer.parseInt(data.substring(294,302),2) / 10f;

	// Destination (302-422)
	destination = super.decode6BitBinary(data.substring(302, 422));
    }

    @Override
    public String print() {
	String outStr = "";
	outStr += "TYPE: " + super.getMsgType() + "\n";
	outStr += "  RI: " + super.getRepeatIndicator() + "\n";
	outStr += "MMSI: " + super.getMMSI() + "\n";
	outStr += " AIS: " + aisVersionInd + "\n";
	outStr += " IMO: " + imoNum + "\n";
	outStr += " C/S: " + callsign + "\n";
	outStr += "NAME: "  + super.getName() + "\n";
	outStr += "TYPE: " + vlTypeCargo + "\n";
	int loa = super.getA() + super.getB();
	int beam = super.getC() + + super.getD();
	outStr += "DIMS: " + loa + "m x " + beam + "m x " + maxDraft + "m\n";
	outStr += "EPFS: " + super.getEpfs() + " (";
	outStr += super.getB() + "m[^] x " + super.getD() +"m[<])\n";
	outStr += "DEST: " + destination + " (" + eta + ")";
	return outStr;
    }



    /**
     * Decodes the vessel type and cargo.
     *
     * @param input         Binary type and cargo data.
     * @since 1.0
     */
    private void decodeVesselTypeCargo(String input){
	switch(Integer.parseInt(input,2)){
            case 0:
                vlTypeCargo = "unspecified";
		break;
            case 21:
		vlTypeCargo = "WIG Haz A";
		break;
            case 22:
		vlTypeCargo = "WIG Haz B";
		break;
            case 23:
		vlTypeCargo = "WIG Haz C";
                break;
            case 24:
		vlTypeCargo = "WIG Haz D";
		break;
            case 30:
		vlTypeCargo = "fishing";
		break;
            case 31:
		vlTypeCargo = "towing";
		break;
            case 32:
		vlTypeCargo = "towing >250m/25m";
		break;
            case 33:
		vlTypeCargo = "dredging/underwater ops";
		break;
            case 34:
		vlTypeCargo = "diving ops";
		break;
            case 35:
		vlTypeCargo = "military ops";
		break;
            case 36:
		vlTypeCargo = "sailing";
		break;
            case 37:
		vlTypeCargo = "pleasure";
		break;
            case 40:
		vlTypeCargo = "HSC";
		break;
            case 41:
		vlTypeCargo = "HSC Haz A";
		break;
            case 42:
		vlTypeCargo = "HSC Haz B";
		break;
            case 43:
		vlTypeCargo = "HSC Haz C";
		break;
            case 44:
		vlTypeCargo = "HSC Haz D";
		break;
            case 50:
		vlTypeCargo = "pilot launch";
		break;
            case 51:
		vlTypeCargo = "SAR craft";
		break;
            case 52:
		vlTypeCargo = "tug";
		break;
            case 53:
		vlTypeCargo = "port tender";
		break;
            case 54:
		vlTypeCargo = "anti-pollution";
		break;
            case 55:
		vlTypeCargo = "law enforcement";
		break;
            case 58:
		vlTypeCargo = "medical transport";
		break;
            case 59:
		vlTypeCargo = "neutral";
		break;
            case 60:
		vlTypeCargo = "passenger";
		break;
            case 61:
		vlTypeCargo = "passenger Haz A";
		break;
            case 62:
		vlTypeCargo = "passenger Haz B";
		break;
            case 63:
		vlTypeCargo = "passenger Haz C";
		break;
            case 64:
		vlTypeCargo = "passenger Haz D";
		break;
            case 70:
		vlTypeCargo = "general cargo";
		break;
            case 71:
		vlTypeCargo = "general cargo Haz A";
		break;
            case 72:
		vlTypeCargo = "general cargo Haz B";
		break;
            case 73:
		vlTypeCargo = "general cargo Haz C";
		break;
            case 74:
		vlTypeCargo = "general cargo Haz D";
		break;
            case 79:
		vlTypeCargo = "general cargo";
		break;
            case 80:
		vlTypeCargo = "tanker";
		break;
            case 81:
		vlTypeCargo = "tanker Haz A";
		break;
            case 82:
		vlTypeCargo = "tanker Haz B";
		break;
            case 83:
		vlTypeCargo = "tanker Haz C";
		break;
            case 84:
		vlTypeCargo = "tanker Haz D";
		break;
            case 90:
		vlTypeCargo = "other";
		break;
            case 91:
		vlTypeCargo = "other Haz A";
		break;
            case 92:
		vlTypeCargo = "other Haz B";
		break;
            case 93:
		vlTypeCargo = "other Haz C";
		break;
            case 94:
		vlTypeCargo = "other Haz D";
		break;
            default:
		vlTypeCargo = "?: " + Integer.parseInt(input,2);
	}
    }

    /**
     * Decodes the vessel's ETA.
     *
     * @param input         Binary ETA data.
     * @since 1.0
     */

    private void decodeETA(String input){
        int month  = Integer.parseInt(input.substring(0,4),2);
	String strMonth;
	switch(month){
            case 1:
                strMonth = "Jan";
                break;
            case 2:
                strMonth = "Feb";
                break;
            case 3:
                strMonth = "Mar";
                break;
            case 4:
                strMonth = "Apr";
                break;
            case 5:
                strMonth = "May";
                break;
            case 6:
                strMonth = "Jun";
                break;
            case 7:
                strMonth = "Jul";
                break;
            case 8:
                strMonth = "Aug";
                break;
            case 9:
                strMonth = "Sep";
                break;
            case 10:
                strMonth = "Oct";
                break;
            case 11:
                strMonth = "Nov";
                break;
            case 12:
                strMonth = "Dec";
                break;
            default:
                strMonth = " ?: " + month;
                break;
        }
        input = input.substring(4);
	int day = Integer.parseInt(input.substring(0,5),2);
	input = input.substring(5);
	int hour = Integer.parseInt(input.substring(0,5),2);
	input = input.substring(5);
	int min = Integer.parseInt(input.substring(0,6),2);
	StringUtil su = new StringUtil();
	eta = su.padNumZero(hour,2) + ":" + su.padNumZero(min, 2) + " on " + su.padNumZero(day,2) + " " + strMonth;
    }

    /**
     * Returns the vessel's destination.
     * 
     * @return destination          The vessel's destination.
     * @since 1.0
     */
    @Override
    public String getDestination() {
        return (destination == null ? "Unknown" : destination);
    }

    /**
     * Returns the vessel's maximum draft.
     * 
     * @return                      The vessel's maximum draft in meters.
     * @since 1.0
     */
    @Override
    public float getMaxDraft() {
        return maxDraft;
    }

    /**
     * Returns the vessel's ETA.
     *
     * @return eta                  The vessel's ETA.
     * @since 1.0
     */
    @Override
    public String getEta(){
        return eta;
    }

    
    /**
     * Returns the vessel's cargo information.
     * 
     * @return cargo                The vessel's type and cargo.
     * @since 1.0
     */
    @Override
    public String getCargoType() {
        return vlTypeCargo;
    }
}
