/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jaid.ais.message;

import java.util.Calendar;
import nav.util.string.StringUtil;


/**
 * Message 4 are base station report messages used for reporting
 * UTC time, date and position. A base station uses Message 4 in its periodical
 * transmissions. For further information see ITU-R M.1371-3.
 * 
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public class AISMsg4 extends AISMsg {

    /* Time fields. */
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;


    /**
     * Constructs a new AIS message type 4.
     *
     * @param data      The AIS feed to parse.
     * @since 1.0
     */
    public AISMsg4(String data) {
        // Message type (bits 0-5)
        super.setMessageType(Integer.parseInt(data.substring(0, 6), 2));

        // Repeat Indicator (6-8)
        super.setRepeatIndicator(Integer.parseInt(data.substring(6, 8), 2));

        // MMSI (9-38)
        StringUtil su = new StringUtil();
        super.setMmsi(su.padNumZero(Integer.parseInt(data.substring(8, 38), 2), 9));

        // UTC Year ()38-52
        year = Integer.parseInt(data.substring(38, 52), 2);

        // UTC Month (52-56)
        month = Integer.parseInt(data.substring(52, 56), 2);

        // UTC Day (56-61)
        day = Integer.parseInt(data.substring(56, 61), 2);

        // UTC Hour (61-66)
        hour = Integer.parseInt(data.substring(61, 66), 2);

        // UTC Minute (66-72)
        minute = Integer.parseInt(data.substring(66, 72), 2);

        // UTC second (72-78)
        second = Integer.parseInt(data.substring(72, 78), 2);

        // Set the time stamp based on above number...
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hour, minute, second);
        super.setTimestamp(cal.getTimeInMillis());
        super.setTimestampStr(su.padNumZero(hour, 2) + ":"
                + su.padNumZero(minute, 2) + ":"
                + su.padNumZero(second, 2));

        // Position Accuracy (78-79)
        super.decodePosAccuracy(data.substring(78, 79));

        // Longitude (79-107) //Latitude (107-134)
        super.decodePosition(data.substring(79, 134));

        // EPFS Type
        super.decodeEPFSType(data.substring(134, 138));
    }

    @Override
    public String print() {
        String outStr = "";
        outStr += "TYPE: " + super.getMsgType() + "\n";
        outStr += " UTC: " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + "\n";
        outStr += "PACC: " + super.getPosAccuracy() + "\n";
        outStr += " LAT: " + super.getPosition().toStringDegMinLat() + "\n";
        outStr += " LNG: " + super.getPosition().toStringDegMinLng() + "\n";
        outStr += "EPFS: " + super.getEpfs();

        return outStr;
    }

    @Override
    public float getSpeed() {
        return AISMsg.NOT_AVAILABLE;
    }

    @Override
    public String getCargoType() {
        return "N/A";
    }

    @Override
    public int getCourse() {
        return AISMsg.NOT_AVAILABLE;
    }

    @Override
    public int getHeading() {
        return AISMsg.NOT_AVAILABLE;
    }
}
