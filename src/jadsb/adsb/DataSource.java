package jadsb.adsb;

import jadsb.adsb.message.ADSBMessage;
import jadsb.adsb.message.MessageFormatter;
import jadsb.data.ActiveAircraftMap;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * ADS-B Data Source.
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class DataSource {

    /* One minute in milliseconds */
    private final static int ONE_MINUTE = 360000;

    /* The IP address of the server that forwards the receiver's messages. */
    private String ipAddress;

    /* The port on which the server that forwards the receiver's messages runs. */
    private int port;
    private boolean isEnabled = true;

    /* Thread maintaining the connection to the server */
    private Thread connectionThread;
    private Socket socket;
    private PrintWriter outDataStrm;
    private BufferedReader inDataStrm;

    public DataSource(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    /**
     * Starts the client (i.e. causes the client to start receiving messages).
     *
     * @since 1.0
     */
    public void start() {
        connectionThread = new Thread(new Runnable() {

            public void run() {
                while (isEnabled) {
                    connect();
                    System.out.println("Try reconnecting...");
                    try {
                        Thread.sleep(20000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }
        });
        connectionThread.start();
    }

    /**
     * Initializes connection with the server. Once connected, all incoming messages
     * will be processed and stored.
     *
     * @since 1.0
     */
    public void connect() {
        try {    
            socket = new Socket(ipAddress, port);
            socket.setSoTimeout(ONE_MINUTE);
                            
            outDataStrm = new PrintWriter(socket.getOutputStream(), true);
            inDataStrm = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String sentence;
            ActiveAircraftMap aircraftMap = ActiveAircraftMap.getInstance();
            MessageFormatter formatter = new MessageFormatter();
            ADSBMessage msg;
            while ((sentence = inDataStrm.readLine()) != null && isEnabled) {
                msg = formatter.extractMessage(sentence);
                if (msg != null) {
                    aircraftMap.assignToAircraft(msg);
                }
//                System.out.println(sentence);
            }
            System.out.println("Connection Terminated");
            outDataStrm.close();
            inDataStrm.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIPAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public static void main(String[] argv) {
        new DataSource("INSERT SERVER IP ADDRESS HERE", 30003).start();
    }
}
