/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg8 extends ADSBMessage {

    public ADSBMsg8(String[] contents) {
        super(contents);
    }

    public boolean isOnGround() {
        if (contents[21].equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    public int getType() {
        return 8;
    }
}
