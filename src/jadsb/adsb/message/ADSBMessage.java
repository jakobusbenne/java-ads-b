/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

/**
 *
 * @author Benjamin Jakobus
 */
public abstract class ADSBMessage {

    protected String[] contents;

    public ADSBMessage(String[] contents) {
        this.contents = contents;
    }

    public String getAircraftID() {
        return contents[3];
    }

    /**
     * Returns the date on which the message was generated.
     *
     * @return
     * @since 2.0
     */
    public String getDate() {
        return contents[6];
    }

    public abstract int getType();

    public String getFlightID() {
        return contents[5];
    }

    /**
     * Returns the time at which the message was generated.
     * @since 2.0
     *
     * @return
     */
    public String getTime() {
        return contents[7];
    }
}
