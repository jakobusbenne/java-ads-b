/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jadsb.adsb.message;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class ADSBMsg1 extends ADSBMessage {

    public ADSBMsg1(String[] contents) {
        super(contents);
    }

    public String getCallsign() {
        return contents[10];
    }

    @Override
    public int getType() {
        return 1;
    }


}
