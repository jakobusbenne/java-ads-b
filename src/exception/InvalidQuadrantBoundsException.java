/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exception;

/**
 * Signals that the <code>Position</code> matrix passed to <code>Quadrant</code>
 * is of a wrong size.
 *
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public class InvalidQuadrantBoundsException extends Exception {

    /* Error message. */
    private String exception;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public InvalidQuadrantBoundsException() {
        super();
        exception = "Position bounds matrix must be of size 2 * 2.";
    }

    @Override
    public void printStackTrace() {
        System.out.println(exception);
        super.printStackTrace();
    }




}
