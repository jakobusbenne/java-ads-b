/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exception;

/**
 * Signals that the <code>Weather</code> instance that is being register
 * at the <code>WeatherRegister</code> has either already been registered or
 * it's name is already in use.
 *
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public class DuplicateWeatherRegistrationException extends Exception {

    /* The error message. */
    private String exception;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public DuplicateWeatherRegistrationException() {
        super();
        exception = "Weather instance already registered or weather ID"
                + "already in use.";
    }

    @Override
    public void printStackTrace() {
        System.out.println(exception);
        super.printStackTrace();
    }
}

