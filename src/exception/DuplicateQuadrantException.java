/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exception;

/**
 * Signals that the <code>Quadrant</code> instance that is being register
 * at the <code>QuadrantRegister</code> has either already been registered or
 * it's name is already in use.
 *
 * @author Benjamin Jakobus
 * @since 1.0
 * @version 1.0
 */
public class DuplicateQuadrantException extends Exception {
    /* The error message. */
    private String exception;

    /**
     * Constructor.
     *
     * @since 1.0
     */
    public DuplicateQuadrantException() {
        super();
        exception = "Quadrant instance already registered or quadrant name"
                + "already in use.";
    }

    @Override
    public void printStackTrace() {
        System.out.println(exception);
        super.printStackTrace();
    }
}

