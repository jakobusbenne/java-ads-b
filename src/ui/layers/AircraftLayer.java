/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.layers;

import gov.nasa.worldwind.layers.RenderableLayer;

/**
 *
 * @author Benjamin Jakobus
 * @since 2.0
 */
public class AircraftLayer extends RenderableLayer {

    private static AircraftLayer targetLayer;

    private AircraftLayer() {
    }

    public static AircraftLayer getInstance() {
        if (targetLayer == null) {
            targetLayer = new AircraftLayer();
        }
        return targetLayer;
    }

}
